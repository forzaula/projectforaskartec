const express = require('express');
const expressSession = require('express-session');
const pgSession = require('connect-pg-simple')(expressSession);
const db= require('./db')
const authRouter= require('./users/authRouter');
const productsRouter=require('./products/productsRouter')
const app=express();
app.use(express.json());




app.use(expressSession({
    store: new pgSession({
        pool : db,                // Connection pool
        tableName : 'session'   // Use another table-name than the default "session" one
        // Insert connect-pg-simple options here
    }),
    createTableIfMissing:true,
    saveUninitialized:true,
    secret: process.env.SESSION_SECRET,
    resave: false,
    cookie: { maxAge: 10 * 60 * 1000 } // 10 minutes
    // Insert express-session options here
}));






app.use("/auth",authRouter);
app.use("/products",productsRouter);

const PORT=process.env.PORT ||4000;







app.listen(PORT,()=>{
    console.log(`server running on ${PORT}`);
})
