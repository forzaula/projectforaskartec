const db= require('../db')
const bcrypt = require("bcrypt");
require("dotenv").config();

class AuthController{
    async registration(req,res){
        let {login,password}=req.body;

        if (!login || !password) {
            return res.status(400).json({message:" заполните все колонки"})}
        if (password.length < 6) {
            return res.status(400).json({message:"длина пароля должна быть больше чем 6"});
        }
        const hashedPassword = await bcrypt.hash(password, 10);
        const user= await db.query('SELECT * FROM USERS WHERE login=$1',[login])
        if(user.rows.length>0){
            return res.status(400).json({message:"такой логин уже есть"})
        }
        db.query(
            `INSERT INTO USERS (login, password)
                VALUES ($1, $2)
                RETURNING id, password`,
            [login, hashedPassword])
        return res.status(400).json({message:"Вы успешно зарегались"})
    }
    async login(req,res){
        const {login,password}=req.body;
        if (!login || !password) {
            return res.status(400).json({message:" заполните все колонки"})}

        const user= await db.query('SELECT * FROM USERS WHERE login=$1',[login])
        if(user.rows.length<1){
            return res.status(400).json({message:" такого юзера нет"})
        }
        const isMatch = await bcrypt.compare(password,user.rows[0].password)
        if(!isMatch){
            return res.status(400).json({message:" пароли не совпадают"})
        }
        req.session.user= user
        req.session.isAuthenticated=true
        res.json(user.rows[0])
    }
    async getAllUsers(req,res){
        const users= await db.query('SELECT * FROM USERS')
        res.json(users.rows)
    }
}

module.exports = new AuthController()