const Router = require('express')
const router = new Router()
const controller = require('./authController')
const isAuth= require('./auth.middleware/auth.midlware')

router.post('/registration',controller.registration)
router.post('/login',controller.login)
router.get('/users',isAuth,controller.getAllUsers)



module.exports = router