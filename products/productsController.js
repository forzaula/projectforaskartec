require("dotenv").config();
const db= require('../db');

class productsController {
    async addProduct(req, res) {
        const {name, category_id} = req.body;
        if (!name || !category_id) {
            return res.status(400).json({message: " заполните все колонки"})
        }

        const product = await db.query('SELECT * FROM PRODUCTS WHERE name=$1', [name])
        if (product.rows.length > 0) {
            return res.status(400).json({message: "продукт с таким именем уже есть"})
        }
        const product2 = await db.query(
            'INSERT INTO products (name,category_id) VALUES ($1, $2) RETURNING *', [name, category_id]
        )
        res.json(product2.rows[0])
    }

    async getAllProducts(req, res) {
        const products = await db.query('SELECT * FROM PRODUCTS')
        res.json(products.rows)
    }

    async deleteProduct(req, res) {
        const id = req.params.id
        const product = await db.query('SELECT * FROM PRODUCTS WHERE id=$1', [id])
        if (product.rows.length < 1) {
            return res.status(400).json({message: "такого продукта нет"})
        }
        await db.query('DELETE FROM products WHERE id = $1', [id])
        return res.status(200).json({message: " пародукт удален"})
    }

    async getProductsByCategory(req, res) {
        const id = req.params.id
        const products = await db.query('SELECT * FROM PRODUCTS WHERE category_id=$1', [id])
        if (products.rows.length < 1) {
            return res.status(400).json({message: "у этой категории пока нет продуктов"})
        }
        res.json(products.rows)
    }

    async getCategories(req, res) {
        const categories = await db.query('SELECT * FROM CATEGORY')
        const products_count=[]
        for (let i of categories.rows){
            const t= await db.query('SELECT * FROM CATEGORY LEFT JOIN products ON CATEGORY.id=products.category_id where category_id=$1',[i.id])
            products_count.push(t.rows.length)
        }
        res.json({
            category:categories.rows,
            products_count:products_count
        })
    }

    async addProductToFavorite(req,res){
        const user_id= req.session.user.rows[0].id
        const product_id= req.params.id
        const product = await db.query('SELECT * FROM PRODUCTS WHERE id=$1', [product_id])
        if (product.rows.length < 1) {
            return res.status(400).json({message: "такого продукта нет"})
        }
        const addedProduct= await db.query('SELECT * FROM user_product_favorite WHERE product_id=$1 and user_id=$2',[product_id,user_id])
        if(addedProduct.rows.length>0){
            return res.status(400).json({message:"этот продукт уже добавлен в избранные"})
        }
        const addProduct = await db.query('insert into user_product_favorite(product_id,user_id) values($1,$2) returning *',[product_id,user_id])
        res.json(addProduct.rows)
    }

    async getProductFavorite(req,res){
        const user_id= req.session.user.rows[0].id;
        const product = await db.query('SELECT * FROM user_product_favorite WHERE user_id=$1', [user_id])
        res.json(product.rows)
    }
}

module.exports = new productsController()
