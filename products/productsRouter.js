const Router = require('express')
const controller = require("../products/productsController");
const router = new Router()
const isAuth= require('../users/auth.middleware/auth.midlware')

router.post('/',isAuth,controller.addProduct);
router.get('/',controller.getAllProducts);
router.delete('/:id',isAuth,controller.deleteProduct);
router.get('/getProductsByCategory/:id',controller.getProductsByCategory);
router.get('/getCategories',controller.getCategories);
router.post('/addProductToFavorite/:id',isAuth,controller.addProductToFavorite);
router.get('/getProductFavorite',isAuth,controller.getProductFavorite)









module.exports = router